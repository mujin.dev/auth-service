package service

import (
	"auth-service/internal/dto"
	"auth-service/internal/model"
	"auth-service/internal/repository"
	"encoding/json"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	*Service
	repository *repository.User
}

func NewUser(dbConnection *gorm.DB, c echo.Context) *User {
	repository := repository.NewUser(dbConnection, c)
	return &User{
		&Service{
			&repository.Repository,
		},
		repository,
	}
}

func (s *User) withTrx(trx *gorm.DB) *User {
	return NewUser(trx, s.repository.C)
}

func (s *User) Login(payload *dto.AuthLoginRequestDTO) (string, error) {
	user, err := s.repository.FindByEmail(payload.Email)
	if err != nil {
		return "", err
	}

	if bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(payload.Password)) != nil {
		return "", err
	}

	token, err := s.generateToken(user)

	return token, nil
}

func (s *User) Create(payload *dto.UserRegisterRequestDTO) (*model.User, error) {

	// parsing to model
	user := new(model.User)
	bytes, err := json.Marshal(&payload)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, &user)
	if err != nil {
		return nil, err
	}
	err = s.Service.Create(user)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *User) LoginByGoogle(payload *dto.UserRegisterRequestDTO) (string, error) {
	logrus.Info("mulai")
	logrus.Info(payload.Email)
	user, err := s.repository.FindByEmail(payload.Email)
	logrus.Info("find email")
	if err != nil {
		if err.Error() != "record not found" {
			logrus.Info("create email")
			user, err = s.Create(payload)
		} else {
			logrus.Info("masuk kondisi error")
			return "", err
		}
	}

	logrus.Info(user, "ini")
	token, err := s.generateToken(user)
	if err != nil {
		return "", err
	}

	return token, err
}

func (s *User) generateToken(user *model.User) (string, error) {
	others := make(map[string]interface{}, 0)
	token, err := user.GenerateToken(others)
	if err != nil {
		return "", err
	}
	return token, err
}
