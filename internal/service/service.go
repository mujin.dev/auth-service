package service

import (
	"auth-service/internal/abstractions"
	res "auth-service/pkg/util/response"
	"errors"
	"reflect"

	"gorm.io/gorm"
)

type Service struct {
	repository *abstractions.Repository
}

func (s *Service) WithTrx(trx *gorm.DB) *Service {
	s.repository.WithTrx(trx)
	return s
}

func (s *Service) Find(payload interface{}, out interface{}) (*abstractions.PaginationInfo, error) {
	var err error

	r := reflect.TypeOf(out)
	if r.Kind() != reflect.Ptr {
		return nil, res.ErrorBuilder(res.Constant.Error.InternalServerError, errors.New("out must be a pointer"))
	}

	info, err := s.repository.Find(payload, out)
	if err != nil {
		if err.Error() == "bad request" {
			return info, res.ErrorBuilder(res.Constant.Error.BadRequest, err)
		}
		return info, res.ErrorBuilder(res.Constant.Error.InternalServerError, err)
	}

	return info, nil
}

func (s *Service) FindById(id int, payload interface{}, out interface{}) error {
	err := s.repository.FindById(id, payload, out)
	if err != nil {
		if err.Error() == "bad request" {
			return res.ErrorBuilder(res.Constant.Error.BadRequest, err)
		} else if err.Error() == "record not found" {
			return res.ErrorBuilder(res.Constant.Error.NotFound, err)
		}
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err)
	}
	return nil
}

func (s *Service) Create(data interface{}) error {
	err := s.repository.Create(data)
	if err != nil {
		if err.Error() == "duplicate data" {
			return res.ErrorBuilder(res.Constant.Error.Duplicate, err)
		}
		return res.ErrorBuilder(res.Constant.Error.UnprocessableEntity, err)
	}
	return nil
}

func (s *Service) Update(data interface{}) error {
	err := s.repository.Update(data)
	if err != nil {
		if err.Error() == "duplicate data" {
			return res.ErrorBuilder(res.Constant.Error.Duplicate, err)
		} else if err.Error() == "record not found" {
			return nil
		}
		return res.ErrorBuilder(res.Constant.Error.UnprocessableEntity, err)
	}
	return nil
}

func (s *Service) Delete(data interface{}) error {
	err := s.repository.Delete(data)
	if err != nil {
		if err.Error() == "record not found" {
			return res.ErrorBuilder(res.Constant.Error.NotFound, err)
		}
		return res.ErrorBuilder(res.Constant.Error.UnprocessableEntity, err)
	}
	return nil
}
