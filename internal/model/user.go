package model

import (
	"auth-service/internal/abstractions"
	"auth-service/internal/config"
	"auth-service/pkg/util"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	abstractions.Model
	Name         string `json:"name"`
	Phone        string `json:"phone"`
	Email        string `json:"email"`
	PasswordHash string `json:"password_hash"`
	Password     string `json:"password"`
	Status       string `json:"status"`
	Picture      string `json:"picture"`
	IsActive     *bool  `json:"is_active" gorm:"default:true"`
	IsVerified   *bool  `json:"is_verified" gorm:"default:false"`
}

func (m *User) BeforeCreate(tx *gorm.DB) (err error) {
	m.DefaultPassword()
	m.HashPassword()
	m.DeletePassword()
	return
}

func (m *User) BeforeUpdate(tx *gorm.DB) (err error) {
	return
}

func (u *User) DefaultPassword() {
	if u.Password == "" {
		u.Password = util.RandomString(9)
	}
}

func (u *User) DeletePassword() {
	u.Password = ""
}

func (u *User) HashPassword() {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.PasswordHash = string(bytes)
}

func (u *User) GenerateToken(others map[string]interface{}) (string, error) {
	jwtKey := config.Config.JWT.Secret

	data := jwt.MapClaims{
		"id": u.ID,
	}

	for k, v := range others {
		data[k] = v
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, data)

	tokenString, err := token.SignedString([]byte(jwtKey))
	return tokenString, err
}
