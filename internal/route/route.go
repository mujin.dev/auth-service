package route

import (
	"auth-service/internal/config"
	"auth-service/internal/modules/auth"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func Init(g *echo.Group) {
	var (
		APP     = config.Config.Server.AppName
		VERSION = config.Config.Server.Version
	)

	g.GET("/", func(c echo.Context) error {
		message := fmt.Sprintf("Welcome to %s version %s", APP, VERSION)
		return c.String(http.StatusOK, message)
	})

	g.GET("/swagger/*", echoSwagger.WrapHandler)

	// routes
	auth.NewHandler().Route(g.Group("/auth"))
}
