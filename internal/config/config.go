package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/jinzhu/configor"
)

type Configuration struct {
	Server       Server     `env:"server"`
	Swagger      Swagger    `env:"swagger"`
	JWT          JWT        `env:"jwt"`
	Databases    []Database `env:"databases"`
	Multitenants []string   `env:"multitenants"`
	Url          Url        `env:"url"`
	Iris         Iris       `env:"iris"`
	Redis        Redis      `env:"redis"`
}

type Server struct {
	ENV          string `env:"env"`
	AppName      string `env:"app_name"`
	AppKey       string `env:"app_key"`
	Port         string `env:"port"`
	Version      string `env:"version"`
	TokenService string `env:"token_service"`
}

type Swagger struct {
	SwaggerScheme string `env:"swagger_scheme"`
	SwaggerPrefix string `env:"swagger_prefix"`
}

type JWT struct {
	Secret string `env:"secret"`
}

type Url struct {
	ServiceApi          string `env:"service_api"`
	ServiceNotification string `env:"service_notification"`
	ServiceAuth         string `env:"service_auth"`
	ServiceCore         string `env:"service_core"`
	Microsite           string `env:"microsite"`
}

type Database struct {
	DBHost        string `env:"db_host"`
	DBUser        string `env:"db_user"`
	DBPass        string `env:"db_pass"`
	DBPort        string `env:"db_port"`
	DBName        string `env:"db_name"`
	DBProvider    string `env:"db_provider"`
	DBSSL         string `env:"db_ssl"`
	DBTZ          string `env:"db_tz"`
	DBAutomigrate bool   `env:"db_automigrate"`
}

type Iris struct {
	BaseUrl        string `env:"base_url"`
	ApiKeyCreator  string `env:"api_key_creator"`
	ApiKeyApprover string `env:"api_key_approver"`
}

type Redis struct {
	Host     string `env:"host"`
	Port     string `env:"port"`
	Password string `env:"password"`
}

var Config *Configuration = &Configuration{}

func Load(path string) error {
	if path == "" {
		path = "config.yml"
	}
	if os.Getenv("ENV") != "development" {
		path = fmt.Sprintf("/run/secrets/%s", os.Getenv("CONFIG"))
	}
	err := configor.New(&configor.Config{AutoReload: true, AutoReloadInterval: time.Minute}).Load(Config, path)
	if err != nil {
		return err
	}

	return nil
}

func (Configuration) String() string {
	sb := strings.Builder{}
	return sb.String()
}

func (c *Configuration) Raw() string {
	bytes, err := json.Marshal(c)
	if err != nil {
		return err.Error()
	}
	return string(bytes)
}
