package repository

import (
	"auth-service/internal/abstractions"
	"auth-service/internal/model"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type User struct {
	abstractions.Repository
}

func NewUser(dbConnection *gorm.DB, c echo.Context) *User {
	Repository := abstractions.NewRepository(dbConnection, &model.User{}, c)

	return &User{
		Repository,
	}
}

func (r *User) FindByEmail(email string) (*model.User, error) {
	logrus.Info(email)
	var data model.User
	err := r.DBConnection.Model(r.Model).
		Where("email = ?", email).
		Where("is_active = ?", true).
		First(&data).Error
	return &data, err
}
