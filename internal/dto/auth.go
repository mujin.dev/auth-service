package dto

// region store
type AuthLoginRequestDTO struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type AuthLoginResponseDTO struct {
	Token string `json:"token"`
}

// endregion

type GoogleStateRequestDTO struct {
	State string `form:"state" json:"state" query:"state"`
	Code  string `form:"code" json:"code" query:"code"`
}
