package dto

import (
	"auth-service/internal/model"
)

type UserData struct {
	model.User
}

type UserRegisterRequestDTO struct {
	Name     string `json:"name" validate:"required"`
	Phone    string `json:"phone"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	Status   string `json:"status"`
	Picture  string `json:"picture"`
}

type UserRegisterResponseDTO struct {
	UserData
}
