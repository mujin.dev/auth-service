package abstractions

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type Repository struct {
	DBConnection *gorm.DB
	Model        interface{}
	C            echo.Context
}

func (r *Repository) WithTrx(trx *gorm.DB) *Repository {
	r.DBConnection = trx.Model(r.Model)
	return r
}

func NewRepository(DBConnection *gorm.DB, Model interface{}, C echo.Context) Repository {
	return Repository{
		DBConnection,
		Model,
		C,
	}
}

func (r *Repository) Find(payload interface{}, out interface{}) (*PaginationInfo, error) {
	var info *PaginationInfo
	var sort string = "created_at desc"
	var err error

	query := r.DBConnection.Model(r.Model)
	payloadNew, ok := payload.(*GetQueries)
	if !ok {
		return nil, errors.New("failed to parsing generic types to queries")
	}

	// pagination
	if payloadNew.Pagination.Page != 0 {
		var limit int = 10
		if payloadNew.Pagination.PageSize != 0 {
			limit = payloadNew.Pagination.PageSize
		}

		var total int64
		var count int64
		query.Count(&total)

		offset := (payloadNew.Pagination.Page - 1) * limit
		query = query.Limit(limit).Offset(offset)
		query.Count(&count)

		info = &PaginationInfo{
			Pagination: &Pagination{
				Page:     payloadNew.Pagination.Page,
				PageSize: payloadNew.Pagination.PageSize,
			},
			Total: total,
			Count: count,
		}
	}

	// sorting
	if payloadNew.Sort != "" {
		query = query.Order(sort)
	}

	// association
	if payloadNew.Associations != "" {
		query, err = r.Association(payloadNew.Associations, query)
		if err != nil {
			return nil, err
		}
	}

	// filter
	if payloadNew.Filter != "" {
		query, err = r.Filter(payloadNew.Filter, query)
		if err != nil {
			return nil, err
		}
	}

	// core
	err = query.Find(out).Error
	if err != nil {
		return nil, err
	}

	return info, nil
}

func (r *Repository) FindById(id int, payload interface{}, out interface{}) (err error) {
	query := r.DBConnection.Model(r.Model)

	if payload != nil {
		payloadNew, ok := payload.(*GetByIdQueries)
		if !ok {
			return errors.New("failed to parsing payload string to queries dto")
		}

		query, err = r.Association(payloadNew.Associations, query)
		if err != nil {
			return err
		}

	}

	err = query.Where("id = ?", id).First(out).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) Create(data interface{}) error {
	return r.DBConnection.Model(r.Model).Create(data).Error
}

func (r *Repository) Update(data interface{}) error {
	bytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	dataMap := make(map[string]interface{})
	err = json.Unmarshal(bytes, &dataMap)
	if err != nil {
		return err
	}

	q := r.DBConnection.
		Model(data).
		Where("id = ?", dataMap["id"]).
		Updates(data)

	err = q.Error
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) Delete(data interface{}) error {
	err := r.DBConnection.Model(r.Model).Delete(data).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) Filter(payload string, query *gorm.DB) (*gorm.DB, error) {
	if payload != "" {
		payload = strings.Replace(payload, "\\", "", -1)

		var M map[string]interface{}
		err := json.Unmarshal([]byte(payload), &M)
		if err != nil {
			return nil, errors.New("bad request")
		}

		for key, value := range M {
			switch v := value.(type) {
			case string:
				query = query.Where(fmt.Sprintf("%s LIKE ?", key), fmt.Sprint("%"+v+"%"))
			default:
				query = query.Where(fmt.Sprintf("%s = ?", key), v)
			}
		}

		return query, nil
	}
	return query, nil
}

func (r *Repository) Association(payload string, query *gorm.DB) (*gorm.DB, error) {
	if payload != "" {
		payload = strings.Replace(payload, "\\", "", -1)

		var M []string
		err := json.Unmarshal([]byte(payload), &M)
		if err != nil {
			return nil, errors.New("bad request")
		}

		for _, value := range M {
			if value != "" {
				query = query.Preload(value)
			}
		}
		return query, nil
	}
	return query, nil
}
