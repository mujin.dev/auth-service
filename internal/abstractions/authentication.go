package abstractions

type AuthClaim struct {
	Token string `json:"token"`
	*Claim
}

type Claim struct {
	Email      string `json:"email"`
	Name       string `json:"name"`
	Phone      string `json:"phone"`
	DBLocation string `json:"db_location"`
	Expire     string `json:"exp"`
}
