package abstractions

import (
	"time"

	"gorm.io/gorm"
)

type Model struct {
	ID int `json:"id" gorm:"primaryKey;autoIncrement;" param:"id"`

	CreatedAt time.Time `json:"created_at"`
	CreatedBy *int      `json:"created_by"`

	ModifiedAt time.Time `json:"modified_at"`
	ModifiedBy *int      `json:"modified_by"`

	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
	DeletedBy *int           `json:"deleted_by"`
}
