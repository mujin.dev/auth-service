package auth

import (
	"auth-service/internal/dto"
	"auth-service/internal/service"
	"auth-service/pkg/util/provider/google"
	res "auth-service/pkg/util/response"
	"encoding/json"
	"errors"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type handler struct {
	Service *service.User
}

func NewHandler() *handler {
	return &handler{}
}

// Login godoc
// @Summary Login user account
// @Description Login user account
// @Tags auth
// @Accept  json
// @Produce  json
// @Param login body dto.AuthLoginRequestDTO true "request body login"
// @Success 200 {object} res.successResponse
// @Failure 400 {object} res.errorResponse
// @Failure 404 {object} res.errorResponse
// @Failure 500 {object} res.errorResponse
// @Router /auth/login [post]
func (h *handler) Login(c echo.Context) (err error) {
	dbConnection, ok := c.Get("db_connection").(*gorm.DB)
	if !ok {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, errors.New("there is no database for your account, Please contact admin handler")).Send(c)
	}
	h.Service = service.NewUser(dbConnection, c)
	// logic
	payload := new(dto.AuthLoginRequestDTO)
	if err = c.Bind(payload); err != nil {
		return res.ErrorBuilder(res.Constant.Error.BadRequest, err).Send(c)
	}
	if err = c.Validate(payload); err != nil {
		return res.ErrorBuilder(res.Constant.Error.Validation, err).Send(c)
	}

	token, err := h.Service.Login(payload)

	if err = c.Validate(payload); err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}

	result := &dto.AuthLoginResponseDTO{
		Token: token,
	}

	return res.SuccessResponse(result).Send(c)
}

// Register godoc
// @Summary Register user account
// @Description Register user account
// @Tags auth
// @Accept  json
// @Produce  json
// @Param login body dto.UserRegisterRequestDTO true "request body register"
// @Success 200 {object} dto.UserRegisterResponseDTO
// @Failure 400 {object} res.errorResponse
// @Failure 404 {object} res.errorResponse
// @Failure 500 {object} res.errorResponse
// @Router /auth/register [post]
func (h *handler) Register(c echo.Context) (err error) {
	dbConnection, ok := c.Get("db_connection").(*gorm.DB)
	if !ok {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, errors.New("there is no database for your account, Please contact admin handler")).Send(c)
	}
	h.Service = service.NewUser(dbConnection, c)
	// logic
	payload := new(dto.UserRegisterRequestDTO)
	if err = c.Bind(payload); err != nil {
		return res.ErrorBuilder(res.Constant.Error.BadRequest, err).Send(c)
	}

	if err = c.Validate(payload); err != nil {
		return res.ErrorBuilder(res.Constant.Error.Validation, err).Send(c)
	}

	user, err := h.Service.Create(payload)
	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}

	result := new(dto.UserRegisterResponseDTO)
	bytes, err := json.Marshal(&user)
	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}
	err = json.Unmarshal(bytes, &result)

	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}

	result.PasswordHash = ""

	return res.SuccessResponse(result).Send(c)
}

func (h *handler) GoogleLogin(c echo.Context) error {
	google.OauthGoogleLogin(c)
	return nil
}

func (h *handler) GoogleLoginCallback(c echo.Context) error {
	dbConnection, ok := c.Get("db_connection").(*gorm.DB)
	if !ok {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, errors.New("there is no database for your account, Please contact admin handler")).Send(c)
	}

	h.Service = service.NewUser(dbConnection, c)

	data, err := google.OauthGoogleCallback(c)
	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}
	logrus.Info(string(data))
	payload := new(dto.UserRegisterRequestDTO)
	err = json.Unmarshal(data, &payload)
	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}

	if payload.Name == "" {
		name := strings.Split(payload.Email, "@")
		payload.Name = name[0]
	}

	logrus.Info(payload)

	token, err := h.Service.LoginByGoogle(payload)
	if err != nil {
		return res.ErrorBuilder(res.Constant.Error.InternalServerError, err).Send(c)
	}

	return res.SuccessResponse(token).Send(c)
}
