package auth

import (
	"auth-service/internal/middlewares"

	"github.com/labstack/echo/v4"
)

func (h *handler) Route(g *echo.Group) {

	path := g.Group("")
	path.Use(middlewares.DbLocator)
	path.POST("/login", h.Login)
	path.POST("/register", h.Register)
	path.GET("/google/callback", h.GoogleLoginCallback)

	path.GET("/google", h.GoogleLogin)
}
