package middlewares

import (
	"auth-service/docs"
	"auth-service/internal/config"
	"fmt"

	"github.com/labstack/echo"
)

func Swaggo(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		docs.SwaggerInfo.Host = c.Request().URL.Host
		docs.SwaggerInfo.Schemes = []string{"http", "https"}
		docs.SwaggerInfo.Host = fmt.Sprintf("%s%s", c.Request().Host, config.Config.Swagger.SwaggerPrefix)
		return next(c)
	}
}
