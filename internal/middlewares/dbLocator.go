package middlewares

import (
	"auth-service/internal/database"
	"errors"
	"strings"

	res "auth-service/pkg/util/response"

	"github.com/labstack/echo/v4"
)

func DbLocator(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var dbLocation string
		// if strings.ToUpper(os.Getenv("ENV")) == "BETA" {
		// 	dbLocation = strings.ToUpper("SYS_AUTH_BETA")
		// } else {S
		// 	dbLocation = strings.ToUpper("SYS_AUTH")
		// }
		dbLocation = strings.ToUpper("AUTH-SERVICE")
		dbConnection, err := database.Connection(dbLocation)
		if err != nil {
			return res.ErrorBuilder(res.Constant.Error.InternalServerError, errors.New("there is no database for your account, please contact admin")).Send(c)
		}
		c.Set("db_connection", dbConnection)
		c.Set("db_location", dbLocation)

		return next(c)
	}
}
