package main

import (
	"auth-service/docs"
	"auth-service/internal/config"
	"auth-service/internal/database"
	"auth-service/internal/route"
	"auth-service/pkg/util"
	"fmt"
	"html/template"
	"net/http"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func init() {

	if os.Getenv("ENV") == "" {
		env := util.NewEnv()
		env.Load()
	}

	err := config.Load("")
	if err != nil {
		logrus.Error(err)
		os.Exit(1)
	}

}

// @title           Swagger Example API
// @version         1.0

func main() {
	var (
		PORT           = config.Config.Server.Port
		APP            = config.Config.Server.AppName
		VERSION        = config.Config.Server.Version
		SWAGGER_PREFIX = config.Config.Swagger.SwaggerPrefix
	)

	database.Init()

	t := &util.Template{
		Templates: template.Must(template.ParseGlob("./public/views/*.html")),
	}

	e := echo.New()

	e.Renderer = t

	//swagger
	docs.SwaggerInfo.Title = APP
	docs.SwaggerInfo.Description = "This is a doc for eclaim-core"
	docs.SwaggerInfo.Version = VERSION
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
	docs.SwaggerInfo.BasePath = SWAGGER_PREFIX
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	//middleware
	e.Pre(
		middleware.Recover(),
		middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: []string{"*"},
			AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodPatch},
		}),
		middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format:           fmt.Sprintf("\n%s | ${host} | ${time_custom} | ${status} | &{latency_human} | ${remote_ip} | ${method} | ${uri}", APP),
			CustomTimeFormat: "2006/01/02 15:04:05",
			Output:           os.Stdout,
		}),
		// middlewares.Swaggo,
	)

	key := "uxYq"
	maxAge := 86400 * 30
	isProd := false

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true
	store.Options.Secure = isProd

	e.Validator = &util.CustomValidator{Validator: validator.New()}

	route.Init(e.Group(""))

	e.Logger.Fatal(e.Start(":" + PORT))
}
